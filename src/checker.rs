use regex::Regex;
use std::fs::File;
use std::io::{self, BufRead, BufReader, Read};
use std::path::Path;

const N_OF_LINES_TO_CONSIDER: usize = 100;

/// A trait to represent how to check for a copyright statement in a line of text.
pub trait Matcher {
    fn is_copyright_line(&self, line: &str) -> bool;
}

impl Matcher for Regex {
    fn is_copyright_line(&self, line: &str) -> bool {
        self.is_match(line)
    }
}

pub fn default_matcher() -> Regex {
    Regex::new("(?i:\\bcopyright\\b)").unwrap()
}

#[derive(Debug)]
pub enum Error {
    MissingCopyright,
    BadFile(io::Error),
}
impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Error::MissingCopyright => write!(f, "missing copyright"),
            Error::BadFile(err) => write!(f, "bad file: {}", err),
        }
    }
}

type Result<T> = std::result::Result<T, Error>;

pub fn check_copyright_with(input: impl Read, matcher: &impl Matcher) -> Result<()> {
    let reader = BufReader::new(input);
    let lines = reader
        .lines()
        .take(N_OF_LINES_TO_CONSIDER)
        .collect::<std::io::Result<Vec<String>>>();
    match lines {
        Err(err) => Err(Error::BadFile(err)),
        Ok(lines) => {
            if lines.is_empty()
                || (lines.len() == 1 && lines[0].is_empty())
                || lines.iter().any(|l| matcher.is_copyright_line(l))
            {
                Ok(())
            } else {
                Err(Error::MissingCopyright)
            }
        }
    }
}

pub fn check_copyright_path_with(path: impl AsRef<Path>, matcher: &impl Matcher) -> Result<()> {
    match File::open(path.as_ref()) {
        Ok(f) => check_copyright_with(f, matcher),
        Err(err) => Err(Error::BadFile(err)),
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn check_for_copyright() {
        let matcher = default_matcher();
        let check_copyright_in_str =
            move |buff: &str| check_copyright_with(buff.as_bytes(), &matcher);

        let text_without_copyright = "line1\nline2\nline3\nline4\nline5\n";
        assert!(check_copyright_in_str(text_without_copyright).is_err());

        let text_with_copyright = "line1\nline2\nline3\na Copyright statement\nline5\n";
        assert!(check_copyright_in_str(text_with_copyright).is_ok());

        let mut many_lines_without_copyright = "line\n".repeat(N_OF_LINES_TO_CONSIDER + 100);
        assert!(check_copyright_in_str(&many_lines_without_copyright).is_err());

        many_lines_without_copyright.push_str("too late for a copyright\n");
        assert!(check_copyright_in_str(&many_lines_without_copyright).is_err());

        let mut many_lines_with_copyright = "line\n".repeat(N_OF_LINES_TO_CONSIDER - 1);
        many_lines_with_copyright.push_str("just in time for a copyright\n\n\n\n");
        assert!(check_copyright_in_str(&many_lines_with_copyright).is_ok());

        assert!(check_copyright_in_str("notacopyright").is_err());

        // empty files are OK
        assert!(check_copyright_in_str("").is_ok());
        assert!(check_copyright_in_str("\n").is_ok());
    }

    #[test]
    fn check_files() {
        let matcher = default_matcher();
        let check_copyright_path = move |p: &str| check_copyright_path_with(p, &matcher);

        assert!(check_copyright_path("Cargo.toml").is_ok());
        assert!(matches!(
            check_copyright_path("Cargo.lock"),
            Err(Error::MissingCopyright)
        ));
        assert!(matches!(
            check_copyright_path("no-such-file"),
            Err(Error::BadFile(_))
        ));
    }
}
