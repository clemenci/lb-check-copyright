mod checker;

use clap::Parser;
use regex::Regex;
use std::{collections::BTreeMap, path::PathBuf};

use checker::{check_copyright_path_with, default_matcher, Error};

#[derive(Parser, Debug)]
#[clap(version, about, author, long_about=None)]
struct Args {
    /// Files to process
    files: Vec<PathBuf>,
    /// Regular expression used to identify properly copyrighted files
    #[clap(short, long, default_value_t = default_matcher())]
    regex: Regex,
}

#[derive(Debug)]
struct Report {
    problems: BTreeMap<PathBuf, Error>,
}
impl std::process::Termination for Report {
    fn report(self) -> std::process::ExitCode {
        use std::process::ExitCode;
        if self.problems.is_empty() {
            ExitCode::SUCCESS
        } else {
            eprintln!("error: some files have problems:");
            for (p, err) in &self.problems {
                eprintln!("- {}: {}", p.display(), err);
            }
            ExitCode::FAILURE
        }
    }
}

fn main() -> Report {
    let args = Args::parse();
    Report {
        problems: args
            .files
            .into_iter()
            .filter_map(|p| {
                check_copyright_path_with(&p, &args.regex)
                    .map_or_else(move |err| Some((p, err)), |_| None)
            })
            .collect(),
    }
}
